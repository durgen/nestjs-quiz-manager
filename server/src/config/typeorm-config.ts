import { User } from './../modules/user/user.entity';
import { Question } from 'src/modules/quiz/entities/question.entity';
import { Quiz } from '../modules/quiz/entities/quiz.entity';
import {
  TypeOrmModuleAsyncOptions,
  TypeOrmModuleOptions,
} from '@nestjs/typeorm';
import { Option } from 'src/modules/quiz/entities/option.entitiy';
import { ConfigService, ConfigModule } from '@nestjs/config';

export const typeOrmAsyncConfig: TypeOrmModuleAsyncOptions = {
  imports: [ConfigModule],
  inject: [ConfigService],
  useFactory: async (): Promise<TypeOrmModuleOptions> => {
    return {
      type: 'mysql',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT),
      username: process.env.DB_USER,
      password: null,
      database: process.env.DB_NAME,
      entities: [Quiz, Question, Option, User],

      // migrations: [__dirname + '/../database/migrations/*{.ts,.js}'],
      // cli: {
      //   migrationsDir: __dirname + '/../database/migrations',
      // },
      extra: {
        charset: 'utf8mb4_unicode_ci',
      },
      synchronize: true,
      logging: true,
    };
  },
};

export default class TypeOrmConfig {
  static getOrmConfig(configService: ConfigService): TypeOrmModuleOptions {
    return {
      type: 'postgres',
      host: configService.get('DB_HOST'),
      port: configService.get('DB_PORT'),
      username: configService.get('DB_USER'),
      password: 'password',
      database: configService.get('DB_NAME'),
      synchronize: true,
      logging: true,
      entities: [Quiz, Question, Option, User],
      // entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      subscribers: [],
      migrations: [],
    };
  }
}

export const typeOrmConfighAsync: TypeOrmModuleAsyncOptions = {
  imports: [ConfigModule],
  useFactory: async (
    configService: ConfigService,
  ): Promise<TypeOrmModuleOptions> => TypeOrmConfig.getOrmConfig(configService),
  inject: [ConfigService],
};
