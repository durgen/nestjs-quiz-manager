import { ApiTokenPaymentException } from './../exception/api-token-payment.exception';
import { JwtService } from '@nestjs/jwt';

import { BadRequestException, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';

export class ApiTokenCheckMiddleware implements NestMiddleware {
  constructor(private jwtService: JwtService) {}
  use(req: Request, res: Response, next: NextFunction) {
    // if (req.headers['api-token'] == 'my-token') {
    //   // throw new BadRequestException('The API token in headers does not match');
    //   // throw new HttpException('My headers', HttpStatus.PAYMENT_REQUIRED);
    //   throw new ApiTokenPaymentException();
    // }
    // let token = null;

    // if (req.headers['authorization']) {
    //   token = req.headers['authorization'];
    // }

    // if (req.headers['x-xsrf-token']) {
    //   token = req.headers['x-xsrf-token'];
    // }

    // if (req.query.token) {
    //   token = req.query.token;
    // }

    // console.log(token);

    // if (token === null) {
    //   throw new BadRequestException();
    // } else {
    //   const parts = token.split(' ');
    //   token = parts.pop();
    // }

    // console.log(req.user);
    next();
  }
}
