import {
  ApiAcceptedResponse,
  ApiBadRequestResponse,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from './jwt-auth.guard';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './local-auth.guard';
import { Controller, Get, Post, Request, UseGuards } from '@nestjs/common';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiAcceptedResponse({ description: 'User login' })
  @ApiBadRequestResponse({ description: 'Unauthorizaed user' })
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req): Promise<any> {
    return this.authService.generateToken(req.user);
  }

  @ApiAcceptedResponse({ description: 'Authenticated User' })
  @ApiBadRequestResponse({ description: 'Unauthorized' })
  @UseGuards(JwtAuthGuard)
  @Get('user')
  async user(@Request() req): Promise<any> {
    return req.user;
  }
}
