import { UserService } from './../user/user.service';
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';

@Injectable()
export class AdminRoleGuard implements CanActivate {
  constructor(private userService: UserService) {}

  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    if (request?.user) {
      const { id } = request.user;
      console.log('id: ', id);
      const user = await this.userService.getUserById(id);
      console.log('User:', user);
      // return user.role == UserRoles.AMDIN;
      // return true;
    }
    return true;
  }
}
