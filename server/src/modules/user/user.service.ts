import { User } from './user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserRegisterRequestDto } from './dto/user-register.req.dto';
// import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async doUserRegistration(
    userRegiseter: UserRegisterRequestDto,
  ): Promise<User> {
    // const salt = await bcrypt.genSalt();
    // const password = await bcrypt.hash(userRegiseter.password, salt);
    const user = new User();
    user.name = userRegiseter.name;
    user.email = userRegiseter.email;
    // user.password = password;
    user.password = userRegiseter.password;
    user.role = userRegiseter.role;

    return await user.save();
  }
  async getUserByEmail(email: string): Promise<User | undefined> {
    // return User.findOne({ where: { email } });
    return await this.userRepository.findOne({ where: { email: email } });
  }

  async getUserById(id: number): Promise<User | undefined> {
    // return User.findOne({ where: { email } });
    return await this.userRepository.findOne({ where: { id: id } });
  }

  async getUsers(): Promise<User[]> {
    return await this.userRepository.find();
  }
}
