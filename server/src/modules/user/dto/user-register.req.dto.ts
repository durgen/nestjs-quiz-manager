import { ApiProperty } from '@nestjs/swagger';
import { REGEX, MESSAGE } from './../../../utils/app.utils';
import { IsEmail, IsNotEmpty, Length, Matches } from 'class-validator';

export class UserRegisterRequestDto {
  @ApiProperty({
    description: 'The name of user',
    example: 'Durgen Rai',
  })
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    description: 'The email address of user',
    example: 'durgen@test.com',
  })
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({
    description: 'The password of user',
    example: 'Kathmandu@123',
  })
  @IsNotEmpty()
  @Length(8, 20)
  @Matches(REGEX.PASSWORD_RULE, { message: MESSAGE.PASSWORD_RULE_MESSAGE })
  password: string;
  @ApiProperty({
    description: 'Comfirm of password',
    example: 'Kathmandu@123',
  })
  @IsNotEmpty()
  @Length(8, 32)
  @Matches(REGEX.PASSWORD_RULE, { message: MESSAGE.PASSWORD_RULE_MESSAGE })
  confirm: string;

  @IsNotEmpty()
  role: string;
  // createdAt: Date;
  // updatedAt: Date;
}
