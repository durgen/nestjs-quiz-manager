import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { SETTING } from './../../utils/app.utils';
import { UserRegisterRequestDto } from './dto/user-register.req.dto';
import { User } from './user.entity';
import { UserService } from './user.service';
import {
  Body,
  Controller,
  Get,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @UsePipes(ValidationPipe)
  @Post('register')
  @ApiCreatedResponse({
    description: 'The user has been successfully created',
    type: User,
  })
  @ApiBadRequestResponse({
    description: 'User cannot register, Try again',
    type: 'User',
  })
  async doUserRegistration(
    @Body(SETTING.VALIDATION_PIPE) userRegiseter: UserRegisterRequestDto,
  ) {
    return await this.userService.doUserRegistration(userRegiseter);
  }

  @Get()
  async getUsers(): Promise<User[]> {
    return await this.userService.getUsers();
  }
}
