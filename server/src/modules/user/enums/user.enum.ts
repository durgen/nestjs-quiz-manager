enum UserRoles {
  AMDIN = 'admin',
  MEMBER = 'member',
}

export { UserRoles };
