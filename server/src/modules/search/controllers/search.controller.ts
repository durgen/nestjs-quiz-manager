import { ApiTags, ApiSecurity } from '@nestjs/swagger';
import { Controller, Get, UseGuards } from '@nestjs/common';
import { SearchService } from '../search.service';
import { JwtAuthGuard } from 'src/modules/auth/jwt-auth.guard';

@ApiTags('Search')
// @ApiSecurity('bearer')
// @UseGuards(JwtAuthGuard)
@Controller('search')
export class SearchController {
  constructor(private searchService: SearchService) {}

  @Get()
  public async getSearch(): Promise<void> {
    const resp = await this.searchService.addDocument([
      {
        id: 1,
        title: 'test test',
      },
      {
        id: 2,
        title: 'test 2',
      },
    ]);
    console.log(resp);
  }
}
