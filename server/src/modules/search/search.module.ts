import { SearchController } from './controllers/search.controller';
import { SearchService } from './search.service';
import { Module } from '@nestjs/common';

@Module({
  providers: [SearchService],
  controllers: [SearchController],
})
export class SearchModule {}
