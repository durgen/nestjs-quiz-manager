import { Injectable } from '@nestjs/common';
import MeiliSearch, { Index } from 'meilisearch';

@Injectable()
export class SearchService {
  private _client: MeiliSearch;

  constructor() {
    this._client = new MeiliSearch({
      host: 'http://127.0.01:7700/',
      apiKey: 'apiKey',
    });
  }

  private getMovieIndex(): Index {
    return this._client.index('movies');
  }

  public async addDocument(documents) {
    const index = this.getMovieIndex();
    return await index.addDocuments(documents);
  }
}
