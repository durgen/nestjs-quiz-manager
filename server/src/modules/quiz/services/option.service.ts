import { Question } from 'src/modules/quiz/entities/question.entity';
import { CreateOptionDto } from './../dto/create-option.dto';
import { Repository } from 'typeorm';
import { Option } from './../entities/option.entitiy';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class OptionService {
  constructor(
    @InjectRepository(Option)
    private optionRepository: Repository<Option>,
  ) {}
  async createOption(option: CreateOptionDto, question: Question) {
    const newOption = await this.optionRepository.save({
      text: option.text,
      isCorrect: option.isCorrect,
    });
    question.options = [...question.options, newOption];
    await question.save();
    return newOption;
  }
}
