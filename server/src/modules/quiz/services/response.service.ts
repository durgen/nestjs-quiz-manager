import { ResponseAddEvent } from './../events/response-add.event';
import { events } from './../../../common/constants/event.constants';
import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';

@Injectable()
export class ResponseService {
  @OnEvent(events.RESPONSE_SUBIMITTED)
  handleIfResponseIsCorrect(payload: ResponseAddEvent) {
    console.log('handleIfResponseIsCorrect', payload);
  }
}
