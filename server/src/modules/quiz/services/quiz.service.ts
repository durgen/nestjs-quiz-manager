import { ResponseAddEvent } from './../events/response-add.event';
import { events } from './../../../common/constants/event.constants';
import {
  IPaginationOptions,
  Pagination,
  paginate,
} from 'nestjs-typeorm-paginate';
import { Option } from 'src/modules/quiz/entities/option.entitiy';
import { Question } from 'src/modules/quiz/entities/question.entity';
import { Quiz } from '../entities/quiz.entity';
import { CreateQuizDto } from '../dto/create-quiz.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OnEvent } from '@nestjs/event-emitter';

@Injectable()
export class QuizService {
  constructor(
    @InjectRepository(Quiz) private quizRepository: Repository<Quiz>, // @InjectRepository(QuizRepository) private quizRepository: QuizRepository,
  ) {}

  async getAllQuiz(): Promise<Quiz[]> {
    // return await this.quizRepository.find({ relations: ['questions'] });
    return await this.quizRepository
      .createQueryBuilder('q')
      // .leftJoinAndSelect(Question, 'question', 'quiz.id=question.quizId')
      .leftJoinAndSelect('q.questions', 'qt')
      .leftJoinAndSelect('qt.options', 'o')

      .getMany();
  }

  async paginate(options: IPaginationOptions): Promise<Pagination<Quiz>> {
    const qb = this.quizRepository.createQueryBuilder('q');
    qb.orderBy('q.id', 'DESC');

    return paginate<Quiz>(qb, options);
  }

  async getQuizById(id: number): Promise<Quiz> {
    return await this.quizRepository.findOne({
      where: { id: id },
      relations: ['questions', 'questions.options'],
    });
  }

  async createNewQuiz(createQuizDto: CreateQuizDto) {
    return await this.quizRepository.save(createQuizDto);
  }

  @OnEvent(events.RESPONSE_SUBIMITTED)
  checkQuizCompeleted(payload: ResponseAddEvent) {
    console.log('checkQuizCompeleted', payload);
  }
}
