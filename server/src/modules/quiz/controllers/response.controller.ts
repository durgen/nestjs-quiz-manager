import { ResponseAddEvent } from './../events/response-add.event';
import { events } from './../../../common/constants/event.constants';
import { ApiTags } from '@nestjs/swagger';
import { Controller, Post } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';

@ApiTags('Response')
@Controller('response')
export class ResponseController {
  constructor(private eventEmitter: EventEmitter2) {}
  @Post()
  async handleQuestionResponse() {
    console.log('This is outside controller');
    const payload = new ResponseAddEvent();
    payload.userId = 1;
    payload.optionId = 33;

    this.eventEmitter.emit(events.RESPONSE_SUBIMITTED, payload);
    return { message: 'Response token' };
  }
}
