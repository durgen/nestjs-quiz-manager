import { RoleGuard } from './../../auth/roles.guard';
import { JwtAuthGuard } from './../../auth/jwt-auth.guard';

import { ApiSecurity } from '@nestjs/swagger';
import { CreateQuizDto } from '../dto/create-quiz.dto';
import { Quiz } from '../entities/quiz.entity';
import { QuizService } from '../services/quiz.service';
import {
  Body,
  Controller,
  DefaultValuePipe,
  Get,
  HttpCode,
  Param,
  ParseIntPipe,
  Post,
  Query,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiPaginatedResponse } from 'src/common/decorator/api-pagination.response';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
import { Roles } from 'src/modules/auth/roles.decorator';

// @ApiTags('Quiz')
@Controller('quiz')
@ApiSecurity('bearer')
@UseGuards(JwtAuthGuard)
export class QuizController {
  constructor(private quizService: QuizService) {}

  @Get('/')
  @ApiPaginatedResponse({ model: Quiz, description: 'List of quiz' })
  async getAllQuiz(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit = 1,
  ): Promise<Pagination<Quiz>> {
    const option: IPaginationOptions = {
      limit: limit,
      page: page,
    };
    return await this.quizService.paginate(option);
  }

  @Post()
  @HttpCode(200)
  @UsePipes(ValidationPipe)
  @UseGuards(RoleGuard)
  @Roles('admin', 'member')
  createNewQuiz(@Body() createQuizDto: CreateQuizDto) {
    return this.quizService.createNewQuiz(createQuizDto);
  }

  @Get(':id')
  async getQuizById(@Param('id', ParseIntPipe) id: number): Promise<Quiz> {
    return await this.quizService.getQuizById(id);
  }
}
