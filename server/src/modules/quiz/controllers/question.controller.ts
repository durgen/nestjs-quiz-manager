import { ApiTags } from '@nestjs/swagger';
import { QuizService } from '../services/quiz.service';
import { Question } from '../entities/question.entity';
import { QuestionService } from '../services/question.service';
import { CreateQuestionDto } from '../dto/create-question.dto';
import {
  Post,
  Controller,
  Body,
  UsePipes,
  ValidationPipe,
  Get,
} from '@nestjs/common';

@ApiTags('Question')
@Controller('question')
export class QuestionController {
  constructor(
    private questionService: QuestionService,
    private quizService: QuizService,
  ) {}

  @UsePipes(ValidationPipe)
  @Post('')
  async saveQuestion(@Body() question: CreateQuestionDto): Promise<Question> {
    const quiz = await this.quizService.getQuizById(question.quizId);
    return this.questionService.createQuestion(question, quiz);
  }

  @Get()
  getQuestions() {
    return this.questionService.getAllQuestion();
  }
}
