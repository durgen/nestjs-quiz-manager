import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, Length } from 'class-validator';

export class CreateQuizDto {
  @ApiProperty({
    description: 'Quiz title',
    example: 'School Level Quiz competition',
  })
  @IsNotEmpty({ message: 'The quiz should have a title' })
  @Length(3, 255)
  title: string;

  @ApiProperty({ description: 'Quia description', example: 'Gread 5 to 10' })
  @IsNotEmpty({ message: 'The quiz should have a description' })
  @Length(3, 255)
  description: string;
}
