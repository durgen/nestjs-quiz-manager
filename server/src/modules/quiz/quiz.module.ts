import { ResponseService } from './services/response.service';
import { ResponseController } from './controllers/response.controller';
import { UserModule } from './../user/user.module';
import { QuestionService } from './services/question.service';
import { Question } from './entities/question.entity';

import { TypeOrmModule } from '@nestjs/typeorm';
import { QuizService } from './services/quiz.service';
import { Module } from '@nestjs/common';
import { QuizController } from './controllers/quiz.controller';
import { QuestionController } from './controllers/question.controller';
import { OptionController } from './controllers/option.controller';

import { OptionService } from './services/option.service';
import { Option } from './entities/option.entitiy';
import { Quiz } from './entities/quiz.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Quiz, Question, Option]), UserModule],
  controllers: [
    QuizController,
    QuestionController,
    OptionController,
    ResponseController,
  ],
  providers: [QuizService, QuestionService, OptionService, ResponseService],
})
export class QuizModule {}
