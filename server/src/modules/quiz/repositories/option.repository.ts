import { Repository, EntityRepository } from 'typeorm';
import { Option } from '../entities/option.entitiy';

@EntityRepository(Option)
export class OptionRepository extends Repository<Option> {}
